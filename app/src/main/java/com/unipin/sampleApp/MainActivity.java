package com.unipin.sampleApp;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;

import com.unipin.unipinsdk.MerchantInfo;
import com.unipin.unipinsdk.comns.model.ChannelData;
import com.unipin.unipinsdk.fragments.UnipinSDKEvent;
import com.unipin.unipinsdk.fragments.UnipinSDKFragment;
import com.unipin.unipinsdk.fragments.UnipinSDKFragment.UnipinSDKContainerListener;
import com.unipin.unipinsdk.network.NetworkInfo;

import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class MainActivity extends AppCompatActivity implements UnipinSDKContainerListener {
    private UnipinSDKFragment frag;
    public static final String TAG = "ALDALI::LOGS";

    @Override
    public void onPause() {

        super.onPause();
    }

    @Override
    protected void onResume(){
        super.onResume();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Map<ChannelData.ChannelTypes, URL> urlMap = new HashMap<>();
        try {
            urlMap.put(ChannelData.ChannelTypes.DCB, new URL("http://merchant.com/callback"));
            urlMap.put(ChannelData.ChannelTypes.OTC, new URL("http://merchant.com/callback"));
            urlMap.put(ChannelData.ChannelTypes.UNIPIN, new URL("http://merchant.com/callback"));
            urlMap.put(ChannelData.ChannelTypes.WEBVIEW, new URL("http://merchant.com/callback"));
        } catch (MalformedURLException ex) {
            //log the exception
        }
        MerchantInfo.createInstance("324D8BF4-F149-720C-1A4D-60815EEEAE6F", "B8st9opV5aud2YAv", urlMap);
//        MerchantInfo.createInstance("AAAAAAAA-BBBB-CCCC-DDDD-EEEEEEEEEEEE", "1234512345123451", urlMap);
        // Get the Network information
        String id = NetworkInfo.getNetworkProviderName(getBaseContext());
        NetworkInfo.snetworkProviderName = id;
        final String phoneNumber = NetworkInfo.getPhoneNumber(getBaseContext());
        NetworkInfo.sphoneNumber = phoneNumber;
        Log.i(TAG, "The Carrier Name is ::: " + id + " The Phone Number is : " + phoneNumber);
//        Toast.makeText(this,"The Carrier Name is: "+ id + "\nThe Phone Number is : " + phoneNumber ,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onContainerEvent(UnipinSDKEvent unipinSDKEvent) {
        switch (unipinSDKEvent) {
            case CLOSE:
                frag.dismiss();
                break;
        }
    }

    public void itemClickListener(View v) {
        int id = v.getId();
        UnipinSDKFragment.ChannelFilter cat = UnipinSDKFragment.ChannelFilter.ALL;
        BigDecimal amt;
        String ref = "YOUR_TRANSACTION_REFERENCE";
        switch (id) {
            // show all the given payment channel from unipin payment SDK
            case R.id.item1:
                amt = new BigDecimal(10_000);
                break;
            case R.id.item2:
                amt = new BigDecimal(100_000);
                break;

            // the particular payment category, available category included :-
            // UNIPIN
            // DCB
            // OTC
            case R.id.item3:
                amt = new BigDecimal(10_000);
                cat = UnipinSDKFragment.ChannelFilter.UNIPIN;
                break;
            case R.id.item4:
                amt = new BigDecimal(15_000);
                cat = UnipinSDKFragment.ChannelFilter.OTC;
                break;

            // the particular payment method, available category included :-
            // UNIPIN_EXPRESS_CHANNEL
            // UNIPIN_WALLET_CHANNEL
            // UPGC_EXPRESS_CHANNEL
            // UPGC_WALLET_CHANNEL
            // TELKOMSEL_CHANNEL
            // XL_CHANNEL
            // INDOSAT_CHANNEL
            // IPAYMENT_CHANNEL
            // ALFAMART_CHANNEL
            case R.id.item5:
                amt = new BigDecimal(10_000);
                cat = UnipinSDKFragment.ChannelFilter.UPGC_EXPRESS_CHANNEL;
                break;
            case R.id.item6:
                amt = new BigDecimal(5_000);
                cat = UnipinSDKFragment.ChannelFilter.TELKOMSEL_CHANNEL;
                break;

            default:
                amt = BigDecimal.ZERO;
        }
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = (int) (metrics.xdpi * 2.7);
        int height = (int) (metrics.ydpi * 4.4);
        frag = UnipinSDKFragment.newInstance(this, cat, amt, ref, width, height);

        openDialog(frag);
    }

    @Override
    public BigDecimal sendNewAmount(BigDecimal savedValue, BigDecimal minAmount, Set<BigDecimal> denom, BigDecimal maxAmount) {
        BigDecimal rtr;
        if (savedValue.compareTo(minAmount) < 0) {
            rtr = minAmount;
        } else if (savedValue.compareTo(maxAmount) > 0) {
            rtr = maxAmount;
        } else {//must be denom
            SortedSet<BigDecimal> sort = new TreeSet<>(denom);
            rtr = sort.tailSet(savedValue).first();
        }
        return rtr;
    }

    private void openDialog(UnipinSDKFragment fragment) {

        FragmentManager frMng = this.getSupportFragmentManager();

        fragment.show(frMng, "payment");
    }
}
